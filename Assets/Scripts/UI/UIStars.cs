﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStars : MonoBehaviour {

    public int m_nStarType = 0;

    public Sprite m_sprStarNormal;
    public Sprite m_sprStarEmpty;

    // type 0
    public Image[] m_aryType0Stars;

    // type 1
    public GameObject[] m_aryType1StarsContainer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetStarLevel( int nLevel )
    {
        if ( m_nStarType == 0 )
        {
            HideAllStars();
            for (int i = 0; i < nLevel; i++ )
            {
                m_aryType0Stars[i].sprite = m_sprStarNormal;
            }
        }
        else if (m_nStarType == 1)
        {
            HideAllStars();
            int nIndex = nLevel - 1;
            if (nIndex >= 0 && nIndex < m_aryType1StarsContainer.Length)
            {
                m_aryType1StarsContainer[nIndex].SetActive(true);
            }
        }
    }


    public void HideAllStars()
    {
        if (m_nStarType == 0)
        {
            for (int i = 0; i < m_aryType0Stars.Length; i++)
            {
                m_aryType0Stars[i].sprite = m_sprStarEmpty;
            }
        }
        else if (m_nStarType == 1)
        {
            for (int i = 0; i < m_aryType1StarsContainer.Length; i++)
            {
                m_aryType1StarsContainer[i].SetActive(false);
            }
        }
    }
} // end class
