﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBlock : MonoBehaviour {

    public Image _imgMain;
    public Outline _outLine;
    public Image _imgBg;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetColor( Color color )
    {
        _imgMain.color = color;
        
    }

    public void SetMaterial( Material mat )
    {
        _imgMain.material = mat;
    }

    public void SetBgSpr( Sprite spr )
    {
        _imgBg.sprite = spr;
    }

} // end class
