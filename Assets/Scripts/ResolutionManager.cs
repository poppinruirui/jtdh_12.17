﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResolutionManager : MonoBehaviour {

    public static ResolutionManager s_Instance = null;

    public enum eResolution
    {
        e_1920_1080,  // common
        e_2436_1125,  // iPhonrX
    };

    public eResolution m_eResolution = eResolution.e_1920_1080;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}




} // end class
