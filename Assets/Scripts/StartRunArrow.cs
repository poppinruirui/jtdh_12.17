﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartRunArrow : MonoBehaviour
{
    public SpriteRenderer m_srMain;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TurnOnOrOff( bool bIsOn )
    {
        m_srMain.gameObject.SetActive( bIsOn );
    }

}
