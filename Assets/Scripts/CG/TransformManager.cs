﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }

    public static void RotateObj( GameObject obj, float fAngle)
    {
        obj.transform.localRotation = Quaternion.identity;
        obj.transform.Rotate(0.0f, 0.0f, fAngle);
    }

} // end class
